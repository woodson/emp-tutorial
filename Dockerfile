FROM gitlab-registry.cern.ch/ssummers/emp-tutorial:1
USER root
ADD y2k22_patch-1.2.zip /opt/Xilinx/
RUN cd /opt/Xilinx && \
    unzip y2k22_patch-1.2.zip && \
    rm -rf y2k22_patch-1.2.zip && \
    export LD_LIBRARY_PATH=$PWD/Vivado/2019.2/tps/lnx64/python-2.7.5/lib/ && \
    Vivado/2019.2/tps/lnx64/python-2.7.5/bin/python2.7 y2k22_patch/patch.py
USER emp-user
RUN sed -i 's/export_design -format ip_catalog/# export_design -format ip_catalog/g' ~/p2fwk-work/src/tutorial/firmware/hls/build.tcl 