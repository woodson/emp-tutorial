# Setup (Getting Vivado)
You can either follow this tutorial using the prepared docker image, or set up on your own system to use later. 
The following instructions include how to:
- Set up a machine on CERN Openstack to run the docker image
- Run the image on your own machine (This might be easier for getting access to the Vivado GUI).
- Set up the required packages and source code to work on your own machine with a local Vivado & Modelsim installation

The slides with the detailed tutorial content are available [here](https://cernbox.cern.ch/index.php/s/FfRMBoXghsdkH6v) (updated 18th November 2021) and the original content presented to the CMG group [here](https://indico.cern.ch/event/961792/).

# CERN Openstack with docker
- Create a CC7 instance with m2.large flavour.
- Also create a volume, sized to around 100 GB, if your quota allows.
- It may take around 10 minutes to provision your VM.
- Once running, open a terminal and ssh to your instance (using your CERN credentials and the name you gave the instance)

### Install and setup docker:

```
sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
sudo yum install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
```

The docker image for the tutorial is large, the 40 GB disk of the m2.large image will not be sufficient.
We already created a volume, we need to set it up and instruct docker to use it.
Attach it to your already created instance (with the web interface or openstack CLI). You will need to format it, then mount it.
From the command line of the VM:

```
sudo mkfs.ext4 /dev/vdb  # change vdb to whichever device your volume attached to from openstack
sudo mount /dev/vdb /mnt # change vdb to whichever device your volume attached to from openstack
```

Then to instruct docker to use it:

```
sudo bash -c 'echo "{\"data-root\":\"/mnt\"}" > /etc/docker/daemon.json'
sudo systemctl restart docker
```

Now we can start docker and pull the image.

```
sudo docker pull gitlab-registry.cern.ch/woodson/emp-tutorial:2
```

Pulling the image should take around 10-15 minutes. Extracting seems to take much longer...

### Start the container

To start a container without the GUI, you can do `sudo docker run -it gitlab-registry.cern.ch/woodson/emp-tutorial:2`.
To start the container, enabling window applications is a bit convoluted, but we would like to use it to inspect the synthesized design.

```bash
VM$ sudo docker run --network host -it -e DISPLAY=$DISPLAY -e COLUMNS="`tput cols`" -e LINES="`tput lines`" -v /tmp/.X11-unix:/tmp/.X11-unix --name emp-tutorial gitlab-registry.cern.ch/woodson/emp-tutorial:2 bash
```

You will enter a terminal, then exit:

```bash
(emp-tutorial) root@VM:~# exit
```

Now copy your `.Xauthority` token to the running container:

```bash
VM$ sudo docker cp ~/.Xauthority emp-tutorial:/home/emp-user/
```

If you don't have a `.Xauthority` file in your home dir, you may need to disconnect and reconnect with `ssh -X`

Then start the container and enter as root:

```bash
VM$ sudo docker start emp-tutorial
VM$ sudo docker exec -it --user root -e DISPLAY=$DISPLAY emp-tutorial bash
```

From there:

```bash
(emp-tutorial) root@hostname:~# chown emp-user:emp-user /home/emp-user/.Xauthority
(emp-tutorial) root@hostname:~# exit
```

Now finally connect as the user:

```bash
sudo docker exec -it -e DISPLAY=$DISPLAY emp-tutorial bash
```

Note the compilation instructions have to be slightly updated from the presentation:
```bash
$ cd ~/p2fwk-work/src/tutorial/firmware/hls
$ vivado_hls -f build.tcl # wait
$ cd ~/p2fwk-work/
$ ipbb proj create vivado my-first-project tutorial: -t my_top.dep
$ cd proj/my-first-project/
$ ipbb vivado make-project
$ ipbb vivado synth -j8 impl -j8
```

# Locally with docker
If you don't have docker already installed, either follow the relevant part of the instructions for CERN Openstack with docker, or on [Docker's web page](https://docs.docker.com/engine/install/).
Then: `docker pull gitlab-registry.cern.ch/woodson/emp-tutorial:2`. The image is around 40 GB, so the download may take some time.
Start a container from the image with:

```bash
docker run -it -v="$HOME/.Xauthority:/home/emp-user/.Xauthority" -e DISPLAY=$DISPLAY gitlab-registry.cern.ch/woodson/emp-tutorial:2
```

If located outside the CERN network, some additional steps will be needed to connect to the license servers at CERN.

# New System Setup Instructions
These steps are a 'one time setup' you need to go through to start simulating and synthesizing firmware with emp-fwk on a new system.

## Setup
This was tested with Ubuntu 18.04

- Install Vivado 2019.2 from web
- Install Modelsim SE-64 2019.2 (from local file, ask me for it! Should generally be obtainable from CERN Mentor representative.)
- Install ipbb (into some Python environment): `pip install https://github.com/ipbus/ipbb/archive/dev/2021i.tar.gz`

## Firmware
Set up an `ipbb` workspace in the home directory like:
```
mkdir p2fwk-work # can be named as you wish.
cd p2fwk-work/
ipbb init .
cd src/
git clone https://gitlab.cern.ch/p2-xware/firmware/emp-fwk -b v0.6.0
git clone https://gitlab.cern.ch/ttc/legacy_ttc -b v2.1
git clone https://github.com/ipbus/ipbus-firmware -b v1.9
```
And for the tutorial content from this repo:
```
git clone https://gitlab.cern.ch/woodson/emp-tutorial tutorial
```

##~/.bashrc
Put the vendor tools on the $PATH
```
source /opt/Xilinx/Vivado/2019.2/settings64.sh
export PATH=$PATH:/opt/Mentor/modeltech/bin
```

Set the environment variables that the vendor tools will use to search for licenses.
If using the tools from outside the CERN network, you will need to do an ssh tunnel & port forward through, e.g. lxplus.
```
export MGLS_LICENSE_FILE=1717@lxlicen01.cern.ch:1717@lxlicen02.cern.ch:1717@lxlicen03.cern.ch
export XILINXD_LICENSE_FILE=2112@licenxilinx
```
You should now be able to work through the examples in the tutorial.
